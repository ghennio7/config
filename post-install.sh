#!/bin/bash

read -r -p 'What is your block device (/dev/sdX)?: ' block_device
read -r -p 'bios or uefi?: ' bios_uefi
read -r -p 'Define the hostname: ' my_hostname
read -r -p 'Define your username: ' my_username
read -r -p 'Portugues(p)/English(e): ' my_locale
read -r -p 'Choose your desktop environment or window manager:
base; kde; gnome; budgie; cinnamon; deepin; ukui; xfce; mate; lxqt; enlightenment; sway; bspwm
: ' de_wm

ln -sf /usr/share/zoneinfo/America/Recife /etc/localtime
hwclock --systohc

while :
	do
		case $my_locale in
			p)
				echo 'pt_BR.UTF-8 UTF-8' >> /etc/locale.gen
				locale-gen
				echo 'LANG=pt_BR.UTF-8' >> /etc/locale.conf
				break
				;;
			e)
				echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen
				locale-gen
				echo 'LANG=en_US.UTF-8' >> /etc/locale.conf
				break
				;;
			*)
				;;
		esac
done

echo "$my_hostname" >> /etc/hostname
echo "127.0.0.1	localhost
::1		localhost
127.0.1.1	$my_hostname.localdomain	$my_hostname" >> /etc/hosts

pacman -S --noconfirm --needed grub os-prober networkmanager neovim git xdg-utils xdg-user-dirs pipewire pipewire-pulse pipewire-alsa pipewire-media-session ntfs-3g noto-fonts noto-fonts-emoji reflector

while :
	do
		case $de_wm in
			base)
				;;
			kde) 
				pacman -S --noconfirm --needed xorg plasma plasma-wayland-session dolphin konsole packagekit-qt5
				systemctl enable sddm
				break
				;;
			gnome)
				pacman -S --noconfirm --needed xorg gnome gnome-software-packagekit-plugin
				systemctl enable gdm
				break
				;;
			budgie)
				pacman -S --noconfirm --needed xorg budgie-desktop budgie-extras budgie-desktop-view budgie-screennsaver gnome-control-center network-manager-applet gnome-software gnome-software-packagekit-plugin gdm
				systemctl enable gdm
				break
				;;
			cinnamon)
				pacman -S --noconfirm --needed xorg cinnamon metacity gnome-software gnome-software-packagekit-plugin sddm
				systemctl enable sddm
				break
				;;
			deepin)
				pacman -S --noconfirm --needed xorg deepin deepin-extra 
				systemctl enable lightdm
				break
				;;
			ukui)
				pacman -S --noconfirm --needed xorg ukui gnome-software gnome-software-packagekit-plugin sddm
				systemctl enable sddm
				break
				;;
			xfce)
				pacman -S --noconfirm --needed xorg xfce4 xfce4-goodies sddm
				systemctl enable sddm
				break
				;;
			mate)
				pacman -S --noconfirm --needed xorg mate mate-extra sddm
				systemctl enable sddm
				break
				;;
			lxqt)
				pacman -S --noconfirm --needed xorg lxqt breeze-icons gvfs gvfs-mtp sddm
				systemctl enable sddm
				break
				;;
			enlightenment)
				pacman -S --noconfirm --needed xorg xorg-xwayland enlightenment terminology packagekit connman sddm
				systemctl enable sddm
				break
				;;
			sway)
				pacman -S --noconfirm --needed xorg sway swaylock swayidle dmenu alacritty
				break
				;;
			bspwm)
				pacman -S --noconfirm --needed xorg xorg-xinit bspwm sxhkd rxvt-unicode alacritty dmenu
				break
				;;
			*)
				;;
		esac
done

while :
	do
		case $bios_uefi in
			bios)
				grub-install --target=i386-pc "$block_device"
				break
				;;
			uefi)
				grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
				break
				;;
			*)
				;;
		esac
done

echo 'GRUB_DISABLE_OS_PROBER=false' >> /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg

timedatectl set-ntp true
systemctl enable reflector.timer
useradd -mG wheel "$my_username"
echo '%wheel      ALL=(ALL) ALL' >> /etc/sudoers

echo
echo 'Done. Set your root and user password.'
